O(Objective):
+ Through Code Review, I learned some common errors in displaying previous pages.
+ Through intra group learning, I understand the role and usage scenarios of asynchronous operations in the front-end.
+ Practice creating Spring Boot projects independently and using a three-layer structure to complete backend projects.
+ I have learned and understood the cross domain links between the front and back ends.
+ Integration testing has been established for the back-end project.
+ Through the afternoon group sharing, I learned about the concept of User Story.

R(Reflective):
+ Fruitful

I(Interpretive):
+ Not very proficient in handling abnormal situations during the front-end and back-end integration process.

D(Decision):
+ Continue practicing and consolidating the knowledge learned today.
