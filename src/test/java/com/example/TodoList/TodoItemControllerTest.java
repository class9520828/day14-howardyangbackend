package com.example.TodoList;

import com.example.TodoList.entity.TodoItem;
import com.example.TodoList.exception.TodoItemNotFoundException;
import com.example.TodoList.repository.JPATodoItemRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoItemControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JPATodoItemRepository jpaTodoItemRepository;

    @BeforeEach
    void setUp() {
        jpaTodoItemRepository.deleteAll();
    }

    @Test
    void should_create_todoItem() throws Exception {
        TodoItem todoItem = new TodoItem("sandasdkajsdkajsd");

        ObjectMapper objectMapper = new ObjectMapper();
        String todoItemRequestJSON = objectMapper.writeValueAsString(todoItem);
        mockMvc.perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoItemRequestJSON))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todoItem.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todoItem.getDone()));
    }

    @Test
    void should_return_todoItems_when_get() throws Exception {
        jpaTodoItemRepository.save(new TodoItem("sadoisdjsaoijd"));

        mockMvc.perform(get("/todos"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(jpaTodoItemRepository.findAll().size()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").isString())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(false));
    }

    @Test
    void should_find_todoItem_by_id() throws Exception {
        TodoItem todoItem = new TodoItem("asiduashdh");
        jpaTodoItemRepository.save(todoItem);

        mockMvc.perform(get("/todos/{id}", todoItem.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todoItem.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todoItem.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todoItem.getDone()));
    }

    @Test
    void should_delete_todoItem_by_id() throws Exception {
        TodoItem todoItemShouldBeDeleted = new TodoItem("sadsdasdadsasdfdg");
        TodoItem todoItem = new TodoItem("sadasdsdasdadssdasdasdasdasdfdg");
        jpaTodoItemRepository.save(todoItemShouldBeDeleted);
        jpaTodoItemRepository.save(todoItem);
        Long todoItemId = todoItemShouldBeDeleted.getId();
        mockMvc.perform(delete("/todos/{id}", todoItemShouldBeDeleted.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));
        assertTrue(jpaTodoItemRepository.findById(todoItemId).isEmpty());
    }

    @Test
    void should_return_updated_todoItem_when_update_todoItem_text_or_done() throws Exception {
        TodoItem previousTodoItem = new TodoItem("sadsd afdfasdfasfa");
        jpaTodoItemRepository.save(previousTodoItem);

        TodoItem todoItemUpdateRequest = new TodoItem("dasdiaosidjaosijdsaidjao");
        todoItemUpdateRequest.setDone(true);

        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(todoItemUpdateRequest);
        mockMvc.perform(put("/todos/{id}", previousTodoItem.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(204));

        Optional<TodoItem> optionalTodoItem = jpaTodoItemRepository.findById(previousTodoItem.getId());
        assertTrue(optionalTodoItem.isPresent());
        TodoItem updatedTodoItem = optionalTodoItem.get();
        Assertions.assertEquals(todoItemUpdateRequest.getText(), updatedTodoItem.getText());
        Assertions.assertEquals(todoItemUpdateRequest.getDone(), updatedTodoItem.getDone());
    }

    @Test
    void should_throw_Exception_when_get_by_id_given_id() throws Exception {
        mockMvc.perform(get("/todos/0"))
                .andExpect(MockMvcResultMatchers.status().is(404))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("The todo id not found."))
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("404"));

    }
}
