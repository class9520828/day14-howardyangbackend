package com.example.TodoList.service;

import com.example.TodoList.entity.TodoItem;
import com.example.TodoList.exception.TodoItemNotFoundException;
import com.example.TodoList.repository.JPATodoItemRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoItemService {
    private final JPATodoItemRepository jpaTodoItemRepository;

    public TodoItemService(JPATodoItemRepository jpaTodoItemRepository) {
        this.jpaTodoItemRepository = jpaTodoItemRepository;
    }

    public List<TodoItem> findAll() {
        return jpaTodoItemRepository.findAll();
    }

    public TodoItem addTodoItem(TodoItem todoItem) {
        return jpaTodoItemRepository.save(todoItem);
    }

    public TodoItem updateTodoItem(Long id, TodoItem todoItem) {
        TodoItem toBeUpdatedTodoItem = jpaTodoItemRepository
                .findById(id)
                .orElseThrow(TodoItemNotFoundException::new);

        if (todoItem.getText() != null) {
            toBeUpdatedTodoItem.setText(todoItem.getText());
        }
        if (todoItem.getDone() != null) {
            toBeUpdatedTodoItem.setDone(todoItem.getDone());
        }

        return jpaTodoItemRepository.save(toBeUpdatedTodoItem);
    }

    public void delete(Long id) {
        jpaTodoItemRepository.deleteById(id);
    }

    public TodoItem findById(Long id) {
        return jpaTodoItemRepository.findById(id)
                .orElseThrow(TodoItemNotFoundException::new);
    }
}
