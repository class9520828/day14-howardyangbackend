package com.example.TodoList.controller;

import com.example.TodoList.entity.TodoItem;
import com.example.TodoList.service.TodoItemService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/todos")
public class TodoItemController {
    private final TodoItemService todoItemService;

    public TodoItemController(TodoItemService todoItemService) {
        this.todoItemService = todoItemService;
    }

    @GetMapping()
    public List<TodoItem> getAllTodoItems() {
        return todoItemService.findAll();
    }

    @PostMapping()
    public TodoItem createTodoItem(@RequestBody TodoItem todoItem) {
        return todoItemService.addTodoItem(todoItem);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public TodoItem updateTodoItem(@PathVariable Long id, @RequestBody TodoItem todoItem) {
        return todoItemService.updateTodoItem(id, todoItem);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodoItem(@PathVariable Long id) {
        todoItemService.delete(id);
    }

    @GetMapping("/{id}")
    public TodoItem getTodoItemById (@PathVariable Long id) {
        return todoItemService.findById(id);
    }

}
