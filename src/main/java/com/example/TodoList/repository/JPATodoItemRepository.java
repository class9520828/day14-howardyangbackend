package com.example.TodoList.repository;

import com.example.TodoList.entity.TodoItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JPATodoItemRepository extends JpaRepository<TodoItem, Long> {

}
